import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/http.service';
import { ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-view-gen-salary',
  templateUrl: './view-gen-salary.component.html',
  styleUrls: ['./view-gen-salary.component.css']
})
export class ViewGenSalaryComponent implements OnInit {
  id:any;
  recentdata: any;
  desgn: any;
  first_name: any;
  last_name: any;
  gross_salary: any;
  month: any;
  year: any;
  basic: number;
  hr: number;
  epf: number;
  sa: number;
  lta: number;
  conveyance: any;
  cea: any;
  medical: any;
  attire: any;
  other: any;
  pt: number;
  esic: any;
  tds: any;
  advance: any;
  total_deduction: number;
  net_salary: number;
  special: number;
  hra: number;
  constructor(public http:HttpService, private _route: ActivatedRoute) { }

  ngOnInit() {
    
   this.fetch();
   
  }

  fetch()
  {
    const month_name=this._route.snapshot.paramMap.get('month')
    const year_name=this._route.snapshot.paramMap.get('year')
      //const g=this.month(id);
      console.log('j')
    this.month=month_name;
    this.year=year_name;
      console.log(this.month)
      console.log(this.year)
   //this.id=localStorage.getItem('id');
   const data={
     month:this.month,
     year:this.year
   }
  
   this.http.post('salary/Salaryslipview',data).subscribe((res:any)=>{
     console.log(res);
     this.recentdata=res.result;
    //this.desgn=this.recentdata[0]['desgn'],
    //  this.first_name=this.recentdata[0]['first_name'],
    //  this.last_name=this.recentdata[0]['last_name'],
    //  this.gross_salary=Math.round(this.recentdata[0]['gross_salary']),
    //  this.month=this.recentdata[0]['month'],
    //  this.year=this.recentdata[0]['year'],
    //  this.basic=Math.round(this.recentdata[0]['basic']);
    //  this.hra=Math.round(this.recentdata[0]['hra']);
     
    //  console.log(this.hra);
    //  this.conveyance=Math.round(this.recentdata[0]['conveyance']);
    //  this.cea=Math.round(this.recentdata[0]['cea']);
    //  this.medical=Math.round(this.recentdata[0]['medical']);
    //  this.attire=Math.round(this.recentdata[0]['attire']);
     
    //  //this.epf=Math.round(this.gross_salary*0.036);
    //  this.special=Math.round(this.recentdata[0]['special']);
    //  this.other=Math.round(this.recentdata[0]['other']);
    //  this.lta=Math.round(this.recentdata[0]['lta']);

    //  this.pt=Math.round(this.recentdata[0]['pt']);
    //  this.esic=Math.round(this.recentdata[0]['esic']);
    //  this.epf=Math.round(this.recentdata[0]['epf']);
    //  this.tds=Math.round(this.recentdata[0]['tds']);
    //  this.advance=Math.round(this.recentdata[0]['advance']);
    //  this.total_deduction=Math.round(this.recentdata[0]['total_deduction']);
    //  this.net_salary=Math.round(this.recentdata[0]['net_salary']);
    //  console.log();
   })
  }

  public ConvertPDFWithWholepage()
  {
    var data=document.getElementById('contentToConvert');
    html2canvas(data).then(canvas=>{
      var imgWidth=208;
      var pageHeight=295;
      var imgHeight=canvas.height * imgWidth / canvas.width;
      var heightLeft=imgHeight;

      const contentDataURL=canvas.toDataURL('image/png')
      let pdf = new jspdf('l', 'pt', "a4");
      var postion=0;
      pdf.addImage(contentDataURL,'PNG',0,postion,imgWidth,imgHeight)
      pdf.save('File.pdf');
    });
  }
}
