import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewGenSalaryComponent } from '../view-gen-salary/view-gen-salary.component';
import { ToastrService } from 'ngx-toastr';
import { UpdategensalaryComponent } from '../updategensalary/updategensalary.component';
import * as XLSX from 'xlsx'; 
//import { UpdateGenSalaryComponent } from '../update-gen-salary/update-gen-salary.component';
@Component({
  selector: 'app-generate-slip',
  templateUrl: './generate-slip.component.html',
  styleUrls: ['./generate-slip.component.css']
})
export class GenerateSlipComponent implements OnInit {
  registerform: FormGroup;
  months;
  years = [];
  currentyr: any;
  recentdata:any;
  selectedAll:any;
  recent:any;
  month:any;
  year:any;
  
  constructor(private formBuilder: FormBuilder,public toastr:ToastrService,
    public dialog: MatDialog,public http:HttpService,private router:Router, private _route: ActivatedRoute) { }

    @ViewChild('TABLE', { static: false }) TABLE: ElementRef;  

  ngOnInit() {
    const month_name=this._route.snapshot.paramMap.get('month')
    const year_name=this._route.snapshot.paramMap.get('year')
      //const g=this.month(id);
      console.log('j')
    this.month=month_name;
    this.year=year_name;
      console.log(this.month)
      console.log(this.year)
    //   const id = +this.route.snapshot.paramMap.get('id');
    // this.getCourse(id);
    
    this.fetchsalaryslip();
    this.fetch();
    this.getDates();
    this.registerform=this.formBuilder.group({
      monthName:['',Validators.required],
      year:['',Validators.required]
    })
  
  }



  fetchsalaryslip()
  {
    console.log('hi');
    console.log(this.month);
    const data={
      month:this.month,
      year:this.year
    }
    this.http.post('salary/getsalslip',data).subscribe((res:any)=>{
      console.log(res);
      this.recentdata=res.result;
      
     });
  }

  getDates() {
    var date = new Date();
    var currentYear = date.getFullYear();
   console.log(date);
    //set values for year dropdown
    for (var i = 0; i <= 5; i++) {
      this.years.push(currentYear - i);
    console.log(this.years[0])
    //this.currentyr=this.years[0];
    }

    //set values for month dropdown
    //id: 3, emp_code: "RAK-19-003"
    this.months = [{name:"Jan",id:1}, {name:"Feb",id:2},{name:"Mar",id:3},{name:"Apr",id:4},
                   {name:"May",id:5},{name:"Jun",id:6},{name:"Jul",id:7},{name:"Aug",id:8},
                   {name:"Sep",id:9},{name:"Oct",id:10},{name:"Nov",id:11},{name:"Dec",id:12}]
  console.log(this.months)
  }

  onSubmit():void
  {
    this.month=this.registerform.value.monthName,
    this.year=this.registerform.value.year
    console.log(this.month);
   console.log(this.registerform.value);
   const data={
    month:this.registerform.value.monthName,
    year:this.registerform.value.year
   }
   this.http.post('salary/CreateSalaryslip',data).subscribe((res:any)=>{
     console.log(res);
     if(res['message']=='Post successfully')
     {
      this.toastr.success('Data Updated successfully!', 'SUCCESS!');
      this.ngOnInit();
     }
     else if(res['message']=='data already exists')
     {
      this.toastr.warning('Duplicate Entry!', 'Warning!');
     }
   })
  }

  // send()
  // {
  //   console.log(this.registerform.value);
   
  // //  const data={
  // //   
  // //   year:this.registerform.value.year
    
  // //  }
  //  this.http.get('salary/showsalslip',{month:this.month}).subscribe((res:any)=>{
  //   console.log(res);
  //   this.recentdata=res.result;
  // });
  // }

  openDialog(eid) {
    console.log(eid);
    localStorage.setItem('id',eid);
   const dialogRef = this.dialog.open(ViewGenSalaryComponent,{
     width: '50%',
     height:'80%',
     
   });

   dialogRef.afterClosed().subscribe(result => {
     
     console.log(`Dialog result: ${result}`);
   });
 }

 openDialog1(eid) {
  console.log(eid);
  localStorage.setItem('id',eid);
 const dialogRef = this.dialog.open(UpdategensalaryComponent,{
   width: '50%',
   height:'80%',
   
 });

 dialogRef.afterClosed().subscribe(result => {
     
  console.log(`Dialog result: ${result}`);
});
}

//  dialogRef.afterClosed().subscribe(result => {
   
//    console.log(`Dialog result: ${result}`);
//  });
// }

selectAll() {
  for (var i = 0; i < this.recentdata.length; i++) {
    this.recentdata[i].selected = this.selectedAll;
    console.log(this.recentdata[i].selected);
  }
}
checkIfAllSelected(eid,checked) {
  console.log(eid);
  console.log(checked);
 if(checked==true)
 {
   console.log('h');
 }
 else{
  console.log('b');
 }
  this.selectedAll = this.recentdata.every(function(item:any) {
    
      return item.selected == true;
      
    })
    
}



delete_emp(eid,month_num,year)
{
  console.log(eid);
  console.log(month_num);
  console.log(year);
  this.http.delete(`salary/DeleteSal/${eid}`).subscribe((res:any)=>{
    console.log(res);
    this.toastr.success('Data Deleted Successfully');
  this.ngOnInit()

  })
}

send()
{
  // for (var i = 0; i < this.recentdata.length; i++) {
  //   this.recentdata[i].selected = this.selectedAll;
  //   console.log(this.recentdata[i].eid);
  // }
  // const data=this.recentdata.filter(data=>{
  //   data.isSelected
  // })
  
  let data=[];
this.recentdata.forEach(x => {
if(x.isSelected) {
data.push(x.eid);
}
})
  console.log(data);
  //console.log(data.selected)
}


ExportTOExcel() {  
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.TABLE.nativeElement);  
  const wb: XLSX.WorkBook = XLSX.utils.book_new();  
  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');  
  XLSX.writeFile(wb, 'ScoreSheet.xlsx');  
}  

fetch()
{
  this.http.get('salary/showSalaryDetailsAll').subscribe((res:any)=>{
    console.log(res);
    this.recent=res.result;
  })
}
team: any = [{  
  Sno: 1,  
  Team: 'India',  
  Match: 8,  
  Win: 7,  
  Loss: 0,  
  Cancel: 1,  
  Point: 15  

}, {  
  Sno: 2,  
  Team: 'NewZeland',  
  Match: 8,  
  Win: 6,  
  Loss: 1,  
  Cancel: 1,  
  Point: 13  

},  
{  
  Sno: '3',  
  Team: 'Aus',  
  Match: 8,  
  Win: 6,  
  Loss: 1,  
  Cancel: 1,  
  Point: 13  

},  
{  
  Sno: '4',  
  Team: 'England',  
  Match: 8,  
  Win: 5,  
  Loss: 2,  
  Cancel: 1,  
  Point: 11  
},  
{  
  Sno: '5',  
  Team: 'S.Africa',  
  Match: 8,  
  Win: 4,  
  Loss: 3,  
  Cancel: 1,  
  Point: 9  
},  
{  
  Sno: '6',  
  Team: 'Pak',  
  Match: 8,  
  Win: 4,  
  Loss: 4,  
  Cancel: 1,  
  Point: 9  

},  
{  
  Sno: '7',  
  Team: 'SriLanka',  
  Match: 8,  
  Win: 3,  
  Loss: 3,  
  Cancel: 2,  
  Point: 8  

},  
{  
  Sno: '8',  
  Team: 'Bdesh',  
  Match: 8,  
  Win: 2,  
  Loss: 4,  
  Cancel: 2,  
  Point: 6  

}  
];  

}  


