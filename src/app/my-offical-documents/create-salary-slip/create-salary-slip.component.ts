import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import { ToastrService } from 'ngx-toastr';
import { ThrowStmt } from '@angular/compiler';
import { iif } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-create-salary-slip',
  templateUrl: './create-salary-slip.component.html',
  styleUrls: ['./create-salary-slip.component.css']
})
export class CreateSalarySlipComponent implements OnInit {
  registerform: FormGroup;
  
  recentdata: any;
  getdata: any;
  fetch: any;
  lname: any;
  fname: any;
  cal: number;
  basic:number;
  hr: number;
  epf: number;
  sa: number;
  lta: number;
  total_earn: number;
  basic_pay: any;
  sum: number;
  conveyance: any;
  cea: any;
  medical: any;
  attire: any;
  special: any;
  other: any;
 v:any;
 net:any;
  grossalary: any;
  netsalary: any;
  constructor(private formBuilder: FormBuilder,public http:HttpService, private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
this.v=1;
    this.fetch_empid();
    this.registerform=this.formBuilder.group({
      fname:[''],
      emp_id:['',Validators.required],
     
       basic_pay:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       hra:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       conveyance:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
        cea:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       lta:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       medical:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       attire:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       special:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
       other:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
      
      gross_salary:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
      pt:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
     epf:['',[Validators.required,Validators.pattern('([0-9]){1,}')]],
     net_salary:[Validators.pattern('([0-9]){1,}')],
    })
 
  }

  onSubmit():void
  {
    
    console.log(this.registerform.value);
    const data={
      empId:this.registerform.value.emp_id,
      basicPay:this.registerform.value.basic_pay,
      hra:this.registerform.value.hra,
      conveyance:this.registerform.value.conveyance,
      cea:this.registerform.value.cea,
      lta:this.registerform.value.lta,
      medical:this.registerform.value.medical,
      attire:this.registerform.value.attire,
      special:this.registerform.value.special,
      other:this.registerform.value.other,
      
      
      // esic:this.registerform.value.esic,
     
      // tds:this.registerform.value.tds,
      // advance:this.registerform.value.advance,
      grossalary:this.registerform.value.gross_salary,
      pt:this.registerform.value.pt,
      epf:this.registerform.value.epf,
      netsalary:(this.registerform.value.gross_salary)-(+this.registerform.value.pt + +this.registerform.value.epf)
    }
    console.log(data);
    this.netsalary=(this.registerform.value.gross_salary)-(+this.registerform.value.pt + +this.registerform.value.epf)
    console.log(this.netsalary);
    this.sum=+this.registerform.value.basic_pay + this.registerform.value.hra + +this.registerform.value.conveyance
    + +this.registerform.value.cea + +this.registerform.value.lta + +this.registerform.value.medical
    + +this.registerform.value.attire + +this.registerform.value.special + +this.registerform.value.other
    this.net=this.registerform.value.gross_salary
    console.log(this.net);
    if(this.sum==this.net)
    {
       if(this.net > this.netsalary && this.netsalary>=0)
  {
    this.spinner.show();//show the spinner
    this.http.post('salary/addsalary',data).subscribe((res:any)=>{
      console.log(res);
      if(res['message']=='Post successfully')
      {
        
        this.toastr.success('Data save successfully!', 'SUCCESS!');
        this.registerform.value.fname='';
        console.log(this.registerform.value.fname);
        this.registerform.reset();
       
      
      }this.spinner.hide();//show the spinner
    })
  }
  else{
    this.toastr.warning('Invalid net salary', 'warning!');
  }
    
  }
  
  else {
    this.toastr.warning('Gross salary and Total earning not match', 'warning!');
  }

  
  
  }

  selectOption(id)
{
  console.log(id);
  this.http.get('emp/vieEmpName',{id:id}).subscribe((res:any)=>{
    console.log(res);
    this.fname=res.result[0].first_name;
    this.lname=res.result[0].last_name;
    //console.log(this.getdata);
  })
  
}

fetch_empid()
{
  this.http.get('emp/getallemp').subscribe((res:any)=>{
    console.log(res);
    this.recentdata=res.result;
  })
}

selectgross(value)
{
console.log(value);
this.basic=Math.round(value*0.3);

this.hr=Math.round(value*0.4);
//this.epf=Math.round(value*0.036);
this.sa=Math.round(value*0.0833);
this.lta=Math.round(value*0.15);

console.log(this.basic);
this.registerform.patchValue({
  basic_pay: this.basic,
  hra: this.hr,
  conveyance: 0,
  cea: 0,
  lta: 0,
  medical: 0,
  attire: 0,
  special: this.sa,
  other: 0,
  // pt: 0,
  // esic: 0,
  // epf: this.epf,
  // tds:0,
  // advance: 0
  
})
}





}
