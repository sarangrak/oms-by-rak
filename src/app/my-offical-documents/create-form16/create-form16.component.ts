import { Component, OnInit,ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator'; 
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-create-form16',
  templateUrl: './create-form16.component.html',
  styleUrls: ['./create-form16.component.css']
})
export class CreateForm16Component implements OnInit {
  registerform: FormGroup;
  recentdata: any;
  fname: any;
  lname:any;
  months;
  years = [];
  getdata: any;
  constructor(private formBuilder: FormBuilder,
    public http:HttpService,
    private toastr: ToastrService,private spinner: NgxSpinnerService,public dialog: MatDialog) { }
  formData=new FormData();

  listData:MatTableDataSource<any>;
    displayedColumns:string[]=['emp_code','first_name','desgn','form16_pdf_path'];
    
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;
    
  ngOnInit() {
    this.fetch();
    
    this.getDates();
    this.showdata();
    this.registerform=this.formBuilder.group({
      // empid:['',Validators.required],
      // emp_name:['',Validators.required],
      emp_id:['',Validators.required],
       year:['',Validators.required],
      form16_pdf_path:['',Validators.required],
    })
  }



  fetch()
  {
    this.spinner.show();//show the spinner
    this.http.get('emp/allEmp').subscribe((res:any)=>{
      console.log(res);
      this.recentdata=res.result;
      this.spinner.hide();
      // if(res['message']=='already')
      // {
      //   this.toastr.warning('Already Created','Warning');
      // }
    })
  }


  selectOption(id)
  {
     console.log(id);
     this.spinner.show();//show the spinner
    this.http.get('emp/vieEmpName',{id:id}).subscribe((res:any)=>{
      console.log(res);
      this.fname=res.result[0].first_name;
      this.lname=res.result[0].last_name;
      //console.log(this.getdata);
      this.spinner.hide();
    })
  }

  getDates() {
    var date = new Date();
    var currentYear = date.getFullYear();
   console.log(date);
    //set values for year dropdown
    for (var i = 0; i <= 5; i++) {
      this.years.push(currentYear - i);
    console.log(this.years[0])
    //this.currentyr=this.years[0];
    }

    //set values for month dropdown
    //id: 3, emp_code: "RAK-19-003"
    this.months = [{name:"Jan",id:1}, {name:"Feb",id:2},{name:"Mar",id:3},{name:"Apr",id:4},
                   {name:"May",id:5},{name:"Jun",id:6},{name:"Jul",id:7},{name:"Aug",id:8},
                   {name:"Sep",id:9},{name:"Oct",id:10},{name:"Nov",id:11},{name:"Dec",id:12}]
  console.log(this.months)
  }

  onSelectedFile2(event)
  {

    console.log(event.target.files);
    if(event.target.files){
    const formData=new FormData();
      this.formData.append('form_16', event.target.files[0]);
    }
    // if(event.target.file)
    // if(event.target.files.length>0)
    // {
    //   const file=event.target.files[0];
    //   this.profileForm.get('ten_Certificate').setValue(file);
     
    // }
  }

  onSubmit():void
  {
    console.log(this.registerform.value);
    //console.log(formData['name']);
    this.formData.append('emp_id',this.registerform.get('emp_id').value);
    this.formData.append('year',this.registerform.get('year').value);
    // this.formData.append('form16_pdf_path',this.registerform.get('form16_pdf_path').value);
    this.spinner.show();//show the spinner
      this.http.post('salary/form16add',this.formData).subscribe((res:any)=>{
        console.log('hi')
    console.log(res);
    if(res['message']=='Post successfully')
    {
      this.toastr.success('Data Save Successfully','Success!');
      this.registerform.reset();
      this.formData = new FormData();
      this.fname = '';
      this.lname = '';
      this.ngOnInit();
    }
    this.spinner.hide();
    }, err=>{
      this.toastr.error(err.message || err);
    })

  }

  showdata()
{
  this.spinner.show();//show the spinner
  this.http.get('leave/formshow16').subscribe((res:any)=>{
    console.log(res);
    var recentvalue=res.result;

      this.listData=new MatTableDataSource(recentvalue);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;
    //this.getdata=res.result;
    this.spinner.hide();
    // this.ngOnInit();
  })
}

applyFilter(filtervalue:string){
  this.listData.filter=filtervalue.trim().toLocaleLowerCase();
  if (this.listData.paginator) {
    this.listData.paginator.firstPage();
  }
}

}
