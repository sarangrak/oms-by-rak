import { Component, OnInit } from '@angular/core';
import { ViewEmpComponent } from 'src/app/employee/view-emp/view-emp.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewSalaryComponent } from '../view-salary/view-salary.component';
import { EditSalaryComponent } from '../edit-salary/edit-salary.component';
import { HttpService } from 'src/app/http.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-show-salary',
  templateUrl: './show-salary.component.html',
  styleUrls: ['./show-salary.component.css']
})
export class ShowSalaryComponent implements OnInit {
  recentdata: any;

  constructor(public dialog: MatDialog,public http:HttpService,
    public toastr:ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();//show the spinner
    this.http.get(`salary/showSalaryDetailsAll`).subscribe((res:any)=>{
      console.log(res);
      this.recentdata=res.result;
      this.spinner.hide();
    })
  }

  openDialog(sid) {
     console.log(sid);
     localStorage.setItem('id',sid);
    const dialogRef = this.dialog.open(ViewSalaryComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialog1(sid)
  {
    console.log(sid);
    localStorage.setItem('id',sid);
    const dialogRef = this.dialog.open(EditSalaryComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
    });
  }

  delete_salary(sid)
  {
    console.log(sid);
    
    
 if(confirm("Are You Sure To Delete Data ?")){
  this.spinner.show();
    this.http.delete(`salary/${sid}`).subscribe((res: any) => {
    console.log(res);
    this.toastr.success('Data Deleted Successfully');
    this.ngOnInit()
    this.spinner.hide();
    });
 }
  }

  

}
