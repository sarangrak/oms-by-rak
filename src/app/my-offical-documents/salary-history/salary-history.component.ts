import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpService } from 'src/app/http.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewGenSalaryComponent } from '../view-gen-salary/view-gen-salary.component';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
@Component({
  selector: 'app-salary-history',
  templateUrl: './salary-history.component.html',
  styleUrls: ['./salary-history.component.css']
})
export class SalaryHistoryComponent implements OnInit {
  recentdata: any;
  getvalue:any;
  month:any;
  year:any;
  constructor(public http:HttpService,public dialog: MatDialog,public toastr:ToastrService,private router:Router,
    private _route: ActivatedRoute,private spinner: NgxSpinnerService) { }

    listData:MatTableDataSource<any>;
    displayedColumns:string[]=['month_name','year','count','totalemp','pdf'];
    
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;


  ngOnInit() {
    

    this.http.get('salary/showsalhistory').subscribe((res:any)=>{
      console.log(res);
      this.recentdata=res.result;
    });

    this.showsalaryslip();
  }

  salarypdf(month,year) 
  {
   console.log(month);
  this.router.navigate(['/view-gen-salary',{month,year}])
 }

 delete_emp(eid,month_num,year)
{
  console.log(eid);
  console.log(month_num);
  console.log(year);
  this.spinner.show();//show the spinner
  this.http.delete(`salary/DeleteSal/${eid}`).subscribe((res:any)=>{
    console.log(res);
    this.toastr.success('Data Deleted Successfully');
  this.ngOnInit()
  this.spinner.hide();//show the spinner
  })
}

showsalaryslip()
{
  this.spinner.show();//show the spinner
  this.http.get(`salary/looksalary_slip`).subscribe((res:any)=>{
    console.log(res);
   //this.getvalue=res.result;
   var recentvalue=res.result;
   this.listData=new MatTableDataSource(recentvalue);
   console.log(this.listData)
   this.listData.sort=this.sort;
   this.listData.paginator=this.paginator;
   this.spinner.hide();//show the spinner
  })
}

applyFilter(filtervalue:string){
  this.listData.filter=filtervalue.trim().toLocaleLowerCase();
  if (this.listData.paginator) {
    this.listData.paginator.firstPage();
  }
}
}
