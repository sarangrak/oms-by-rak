import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpService } from 'src/app/http.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator'; 
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-emp-leave-details',
  templateUrl: './emp-leave-details.component.html',
  styleUrls: ['./emp-leave-details.component.css']
})
export class EmpLeaveDetailsComponent implements OnInit {
  recentdata: any;
  constructor(public http:HttpService,private spinner: NgxSpinnerService) { }

  listData:MatTableDataSource<any>;
  displayedColumns:string[]=['emp_code','first_name','desgn','sl_taken','cl_taken','total-taken','sl_balance'
,'cl_balance','total-balance'];

  @ViewChild(MatSort,null) sort:MatSort;
  @ViewChild(MatPaginator,null) paginator:MatPaginator;

  ngOnInit() {
this.fetch();
    
  }

  fetch()
  {
    this.spinner.show();//show the spinner
    this.http.get('leave/catchLeaveDetailsAll').subscribe((res:any)=>{
      console.log(res);
      var recentvalue=res.result;
      this.listData=new MatTableDataSource(recentvalue);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;
      this.spinner.hide();
    });
  }

  applyFilter(filtervalue:string){
    this.listData.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }


}
