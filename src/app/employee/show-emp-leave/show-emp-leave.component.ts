import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpService } from 'src/app/http.service';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-show-emp-leave',
  templateUrl: './show-emp-leave.component.html',
  styleUrls: ['./show-emp-leave.component.css']
})
export class ShowEmpLeaveComponent implements OnInit {
  recentdata: any;
  recentdata1:any;
  id: any;
  yes: string;
  acpt: Boolean = true; 
  rejt: Boolean = true;
  designation: any;
  constructor(public http:HttpService,private spinner: NgxSpinnerService) { }

  listData:MatTableDataSource<any>;
  listData1:MatTableDataSource<any>;
    displayedColumns:string[]=['emp_code','first_name','desgn','type','from_date','to_date','total_days','reason','id'];
    
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;

  ngOnInit() {
    this.fetch();
    this.fetch1();
    this.designation=localStorage.getItem('designation');
  }

  update(id)
  {
    if(confirm("Are you sure to reject leave?")){
    console.log(id);
    const data={
     status:'Rejected'
   }
   this.spinner.show();//show the spinner
      this.http.put(`leave/rejectLeave`,data,{id:id}).subscribe((res:any)=>{
      console.log(res);
      this.ngOnInit();
      this.spinner.hide();
    });
  }
  }

  accept(id,type,total_days,eid)
  {
    console.log(id);
    console.log(type);
    console.log(total_days);
    console.log(eid);
    if(confirm("Are you sure to accept leave?")){
      console.log('hi');
    const data={
      id:id,
      type:type,
      totalDays:total_days,
      eid:eid,
    } 
    console.log('hi');
    this.spinner.show();//show the spinner
    this.http.post('leave/accept',data).subscribe((res:any)=>{
      console.log(res);
      
      if(res['message']=='Leave Accepted successfully')
      {
       this.acpt=true;
        this.rejt=false;
        this.ngOnInit();
      }
      this.spinner.hide();
    })
  }
  }

  fetch()
  {
    this.spinner.show();//show the spinner
    this.http.get('leave/LeaveAppliAlllook').subscribe((res:any)=>{
      console.log(res);
      var recentdata=res.result;
      this.listData=new MatTableDataSource(recentdata);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;
      this.spinner.hide();
    });
  }

  

  applyFilter(filtervalue:string){
    this.listData.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }

  fetch1()
  {
    this.spinner.show();//show the spinner
    this.http.get('leave/lookLeaveAppliAll').subscribe((res:any)=>{
      console.log('hi')
      console.log(res);
      var recentdata1=res.result;
      this.listData1=new MatTableDataSource(recentdata1);
      console.log(this.listData1)
      this.listData1.sort=this.sort;
      this.listData1.paginator=this.paginator;
      this.spinner.hide();
    });
  }

  applyFilter1(filtervalue:string){
    this.listData1.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData1.paginator) {
      this.listData1.paginator.firstPage();
    }
  }

}
