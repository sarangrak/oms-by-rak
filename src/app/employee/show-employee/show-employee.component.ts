import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator'; 
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewEmpComponent } from '../view-emp/view-emp.component';
import { EditEmployeeComponent } from '../edit-employee/edit-employee.component';
import { HttpService } from 'src/app/http.service';
import { ToastrService } from 'ngx-toastr';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-show-employee',
  templateUrl: './show-employee.component.html',
  styleUrls: ['./show-employee.component.css']
})
export class ShowEmployeeComponent implements OnInit {
  showemp: any;
  
  constructor(public router:Router,public dialog: MatDialog,public http:HttpService,
    public toastr:ToastrService,private spinner: NgxSpinnerService) { }

    listData:MatTableDataSource<any>;
    displayedColumns:string[]=['first_name','mobile_no','personal_email','desname','id'];
    
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;

  ngOnInit() {
    this.fetch();
  }

  
  openDialog(id) {
    console.log(id);
    localStorage.setItem('id',id);
    const dialogRef = this.dialog.open(ViewEmpComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
    });
  }

  openDialog1(id)
  {
    localStorage.setItem('id',id);
    const dialogRef = this.dialog.open(EditEmployeeComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
    });
  }

  

  delete_emp(id)
  {
    //alert(id);
 console.log(id);
 if(confirm("Are You Sure To Delete Data ?")){
  this.spinner.show();//show the spinner
    this.http.delete(`emp/${id}`).subscribe((res: any) => {
    console.log(res);
    this.toastr.success('Data Deleted Successfully');
    this.ngOnInit()
    this.spinner.hide();
    });
 }
 
  }

  fetch()
  {
    // this.http.get(`emp/allEmp`).subscribe((res:any)=>{
    //   console.log(res);
    //   this.showemp=res.result;
    // });
    this.spinner.show();//show the spinner
    this.http.get('emp/Empallview').subscribe((res:any)=>{
      console.log(res);
      var recentvalue=res.result;
      this.listData=new MatTableDataSource(recentvalue);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;

      this.spinner.hide();
    });
  }
  applyFilter(filtervalue:string){
    this.listData.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }

}
