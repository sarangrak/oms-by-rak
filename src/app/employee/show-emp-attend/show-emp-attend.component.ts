import { Component, OnInit,ViewChild } from '@angular/core';
import { HttpService } from 'src/app/http.service';
import {formatDate } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator'; 
import { MatSort } from '@angular/material/sort';
import { EditattendComponent } from '../editattend/editattend.component';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-show-emp-attend',
  templateUrl: './show-emp-attend.component.html',
  styleUrls: ['./show-emp-attend.component.css']
})
export class ShowEmpAttendComponent implements OnInit {
  recentvalue: any;
  date='';
  constructor(public http:HttpService,public dialog: MatDialog,public toastr:ToastrService
    ,private spinner: NgxSpinnerService) { }

  listData:MatTableDataSource<any>;
    displayedColumns:string[]=['emp_code','fullname','date','check_in','check_out','worked_hours',
  'attendance','id'];
    
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;


  ngOnInit() {
    //this.fetch();
    this.spinner.show();//show the spinner
    this.http.get('leave/showAllattend').subscribe((res:any)=>{
      console.log(res);
      var recentvalue=res.result;
      this.listData=new MatTableDataSource(recentvalue);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;
      this.spinner.hide();
    });
  }

  
  applyFilter(filtervalue:string){
    this.listData.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }

  update(id)
  {
    console.log(id);
    localStorage.setItem('id',id);
    const dialogRef = this.dialog.open(EditattendComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
      this.ngOnInit();
    });
  }

  

  delete_emp(id)
  {
    //alert(id);
 console.log(id);
 if(confirm("Are You Sure To Delete Data ?")){
    this.http.delete(`leave/${id}`).subscribe((res: any) => {
    console.log(res);
    this.toastr.success('Data Deleted Successfully');
    this.ngOnInit()
    });
 }
 
  }

}
