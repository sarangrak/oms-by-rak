import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/http.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  recentvalue: any;
  join_date: any;
  emp_code: any;
  desname: any;
  first_name: any;
  last_name: any;
  address: any;
  mobile_no: any;
  alternate_mobile_no: any;
  personal_email: any;
  professional_email: any;
  gender: any;
  pan: any;
  
  qualification: any;
  aadhaar: any;
  dob: any;

  constructor(public http:HttpService) { }

  ngOnInit() {
    this.fetch();
  }

  fetch()
  {
    this.http.get('leave/profileview').subscribe((res:any)=>{
      console.log(res);
      this. recentvalue=res.result;
      this.join_date=res.result[0]['doj'],
      this.emp_code=res.result[0]['emp_code'],
      this.desname=res.result[0]['desname'],
      this.first_name=res.result[0]['first_name'],
      this.last_name=res.result[0]['last_name'],
      this.address=res.result[0]['address'],
      this.mobile_no=res.result[0]['mobile_no'],
      this.alternate_mobile_no=res.result[0]['alternate_mobile_no'],
      this.personal_email=res.result[0]['personal_email'],
      this.professional_email=res.result[0]['professional_email'],
      this.gender=res.result[0]['gender'],
      this.pan=res.result[0]['pan'],
      this.aadhaar=res.result[0]['aadhaar'],
      this.qualification=res.result[0]['qualification'],
      this.dob=res.result[0]['dob'],
      console.log('hee');
      console.log(this.join_date);
    })
  }
}
