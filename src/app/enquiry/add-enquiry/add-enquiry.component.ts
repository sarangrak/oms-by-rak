import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator'; 
import { ToastrService } from 'ngx-toastr';
import { MatSort } from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewEnquiryComponent } from '../view-enquiry/view-enquiry.component';
import { UpdateEnquiryComponent } from '../update-enquiry/update-enquiry.component';

@Component({
  selector: 'app-add-enquiry',
  templateUrl: './add-enquiry.component.html',
  styleUrls: ['./add-enquiry.component.css']
})
export class AddEnquiryComponent implements OnInit {
  registerform: FormGroup;
  registerform1:FormGroup;
  registerform2:FormGroup;
  registerform3:FormGroup;
  value: any;
  get_enquiry: any;
  get_reference: any;
  get_state: any;
  getdata: any;
 
  followdate: Boolean = false;
 // listData:MatTableDataSource<any>;
  constructor(private dialog: MatDialog,private formBuilder: FormBuilder,public http:HttpService,
    private toastr: ToastrService) { 
      
    }

    listData:MatTableDataSource<any>;
    displayedColumns:string[]=['name','email','enquiry_name','status','id'];
    //displayedColumns:string[]=['email'];
    
    @ViewChild(MatSort,null) sort:MatSort;
    @ViewChild(MatPaginator,null) paginator:MatPaginator;
  ngOnInit() {
    this.fetch1();
     this.enquiry_type();
     this.reference();
     this.state();
    this.registerform=this.formBuilder.group({
      reference:['',Validators.required],
      stud_name:['',Validators.required],
      contact_no:['',[Validators.required, Validators. pattern('([0-9]){10}')
    ]],
      email:['',[Validators.required, Validators.email]],
      qualification:['',Validators.required],
      address:['',Validators.required],
      fees:['',Validators.required],
      status:['',Validators.required],
      followdate:['']
    })
    this.registerform1=this.formBuilder.group({
      reference:['',Validators.required],
       project_name:['',Validators.required],
       cust_name:['',Validators.required],
       contact_no:['',[Validators.required, Validators. pattern('([0-9]){10}')
      ]],
       email:['',[Validators.required, Validators.email]],
       company_name:['',Validators.required],
       address:['',Validators.required],
       status:['',Validators.required],
       
    })
   
    this.registerform2=this.formBuilder.group({
      emp_name:['',Validators.required],
        contact_no:['',[Validators.required, Validators. pattern('([0-9]){10}')
      ]],
        email:['',[Validators.required, Validators.email]],
       company_name:['',Validators.required],
        address:['',Validators.required],
       status:['',Validators.required],
       ctc:['',[Validators.required, Validators. pattern('([0-9]){2,}')
      ]],
       design:['',Validators.required],
      
       gstin:['',[Validators.required, Validators. pattern('([0-9]){2,}')
      ]],
       state:['',Validators.required],
       purchase_order:['',Validators.required],
    })
    this.registerform3=this.formBuilder.group({
      emp_name:['',Validators.required],
      // stud_name:['',Validators.required],
      contact_no:['',[Validators.required, Validators. pattern('([0-9]){10}')
    ]],
       email:['',[Validators.required, Validators.email]],
       company_name:['',Validators.required],
       address:['',Validators.required],
      status:['',Validators.required],
       ctc:['',[Validators.required, Validators. pattern('([0-9]){2,}')
      ]],
       salary:['',[Validators.required, Validators. pattern('([0-9]){2,}')
      ]],
       design:['',Validators.required],
      
    })
   
  }

  onSubmit():void
  {
     console.log(this.registerform.value);
     const data={
      reference:this.registerform.value.reference,
      studName:this.registerform.value.stud_name,
      contactNo:this.registerform.value.contact_no,
      email:this.registerform.value.email,
      qualification:this.registerform.value.qualification,
      address:this.registerform.value.address,
      fees:this.registerform.value.fees,
      status:this.registerform.value.status,
      followdate:this.registerform.value.followdate,
      }
      console.log(data);
     this.http.post('enquiry/add-intern',data).subscribe((res:any)=>{
       console.log(res);
       if(res['message']=='Post successfully')
       {
        this.toastr.success('Data save successfully!', 'SUCCESS!');
        this.registerform.reset();
        this.ngOnInit();
       }
     })
  }

  onsubmit1():void
  {
     console.log(this.registerform1.value);
     const data={
      reference:this.registerform1.value.reference,
      projectName:this.registerform1.value.project_name,
      custName:this.registerform1.value.cust_name,
      contactNo:this.registerform1.value.contact_no,
      email:this.registerform1.value.email,
      companyName:this.registerform1.value.company_name,
      address:this.registerform1.value.address,
      status:this.registerform1.value.status,
      }
      console.log(data);
     this.http.post('enquiry/project-add',data).subscribe((res:any)=>{
       console.log(res);
       if(res['message']=='Post successfully')
       {
        this.toastr.success('Data save successfully!', 'SUCCESS!');
        this.registerform1.reset();
        this.ngOnInit();
       }
     })
  }

  onsubmit2():void
  {
     console.log(this.registerform2.value);
     const data={
      contactNo:this.registerform2.value.contact_no,
      email:this.registerform2.value.email,
      companyName:this.registerform2.value.company_name,
      address:this.registerform2.value.address,
      status:this.registerform2.value.status,
      ctc:this.registerform2.value.ctc,
      design:this.registerform2.value.design,
      gstin:this.registerform2.value.gstin,
      state:this.registerform2.value.state,
      purchaseOrder:this.registerform2.value.purchase_order,
      
      }
      console.log(data);
     this.http.post('enquiry/recuritadd',data).subscribe((res:any)=>{
       console.log(res);
       if(res['message']=='Post successfully')
       {
        this.toastr.success('Data save successfully!', 'SUCCESS!');
        this.registerform2.reset();
        this.ngOnInit();
       }
     })
  }

  onsubmit3():void
  {
     console.log(this.registerform3.value);
     const data={
      contactNo:this.registerform3.value.contact_no,
      email:this.registerform3.value.email,
      companyName:this.registerform3.value.company_name,
      address:this.registerform3.value.address,
      status:this.registerform3.value.status,
      ctc:this.registerform3.value.ctc,
      salary:this.registerform3.value.salary,
      design:this.registerform3.value.design,
      
      }
      console.log(data);
     this.http.post('enquiry/staffadd',data).subscribe((res:any)=>{
       console.log(res);
       if(res['message']=='Post successfully')
       {
        this.toastr.success('Data save successfully!', 'SUCCESS!');
        this.registerform3.reset();
        this.ngOnInit();
       }
     })
  }

  select(value)
  {  
    console.log(value);
    this.value=value;
  }

  fetch()
  {
    this.http.get(`emp/allEmp`).subscribe((res:any)=>{
      console.log(res);
      
  
    });
  }

  enquiry_type()
  {
    this.http.get('enquiry/showenquiry-type').subscribe((res:any)=>{
      //console.log(res);
      this.get_enquiry=res.result;
      console.log(this.get_enquiry);
    })
  } 
  fetch1()
  {
    this.http.get('enquiry/viewenquiry').subscribe((res:any)=>{
      console.log(res);
      var recentvalue=res.result;
      this.listData=new MatTableDataSource(recentvalue);
      console.log(this.listData)
      this.listData.sort=this.sort;
      this.listData.paginator=this.paginator;
    });
  }

  applyFilter(filtervalue:string){
    this.listData.filter=filtervalue.trim().toLocaleLowerCase();
    if (this.listData.paginator) {
      this.listData.paginator.firstPage();
    }
  }

  reference()
  {
    this.http.get('enquiry/show-reference').subscribe((res:any)=>{
      //console.log(res);
      this.get_reference=res.result;
      console.log(this.get_reference);
    })
  }

  state()
  {
    this.http.get('enquiry/show-state').subscribe((res:any)=>{
      //console.log(res);
      this.get_state=res.result;
      console.log(this.get_state);
    })
  }

  openDialog(id) {
    console.log(id);
    localStorage.setItem('id',id);
   const dialogRef = this.dialog.open(ViewEnquiryComponent,{
     width: '50%',
     height:'80%',
     
   });

   dialogRef.afterClosed().subscribe(result => {
     
     console.log(`Dialog result: ${result}`);
   });
 }

 openDialog1(id)
  {
    console.log(id);
    localStorage.setItem('id',id);
    const dialogRef = this.dialog.open(UpdateEnquiryComponent,{
      width: '50%',
      height:'80%',
      
    });

    dialogRef.afterClosed().subscribe(result => {
      
      console.log(`Dialog result: ${result}`);
    });
  }

  delete(id)
  {
    console.log(id);
    if(confirm("Are You Sure To Delete Data ?")){
      this.http.delete(`enquiry/${id}`).subscribe((res: any) => {
      console.log(res);
      this.toastr.success('Data Deleted Successfully');
      this.ngOnInit()
      });
   }
  }

  checkstatus(value)
  {
  console.log(value);
  if(value=='Inprocess')
  {
    this.followdate=true
  }
  else if(value=='Confirmed')
  {
    this.followdate=false
  }

  }
  
  
}

