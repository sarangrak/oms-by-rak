import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEnquiryComponent } from './add-enquiry/add-enquiry.component';
import { SlizzingModule } from '../slizzing/slizzing.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatPaginatorModule} from '@angular/material/paginator';
import { ViewEnquiryComponent } from './view-enquiry/view-enquiry.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UpdateEnquiryComponent } from './update-enquiry/update-enquiry.component';

@NgModule({
  declarations: [AddEnquiryComponent, ViewEnquiryComponent, UpdateEnquiryComponent],
  imports: [
    CommonModule,
    SlizzingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,MatSortModule,
    MatFormFieldModule,MatPaginatorModule,
    MatDialogModule
  ],
  exports:[
    AddEnquiryComponent
  ]
})
export class EnquiryModule { }
