import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-apply-leave',
  templateUrl: './apply-leave.component.html',
  styleUrls: ['./apply-leave.component.css']
})
export class ApplyLeaveComponent implements OnInit {
  registerform: FormGroup;
  showleave: any;
  recentdata: any;
  username: string;
  emp_id: string;
  
  constructor(private formBuilder: FormBuilder,public http:HttpService,private toastr: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
   this.fetchleave();
   this.emp_id=localStorage.getItem('emp_id');
    this.http.get(`leave/showleave`,{emp_id:this.emp_id}).subscribe((res:any)=>{
      console.log(res);
      this.showleave=res.result;
    });

    //this.fetchleave();
    this.registerform=this.formBuilder.group({
      from_date:['',Validators.required],
      to_date:['',Validators.required],
      type:['',Validators.required],
      reason:['',Validators.required],
    })
  }

  onSubmit():void
  {
    const data={
      fromDate:this.registerform.value.from_date,
      toDate:this.registerform.value.to_date,
      type:this.registerform.value.type,
      reason:this.registerform.value.reason,
    }
    console.log(this.registerform.value);
    this.spinner.show();//show the spinner
    this.http.post('leave/addleave',data).subscribe((res:any)=>{
      console.log(res);
      if(res['message']=='Post successfully')
       {
         console.log('save');
         this.toastr.success('Data save successfully!', 'SUCCESS!');
         this.registerform.reset();
         this.ngOnInit();
       }
       else{
        this.toastr.error('Something Wrong!', 'Error!');
        console.log('not');
       }
       this.spinner.hide();
    })
  }

  fetchleave()
  {
    this.username=localStorage.getItem('username');
   
    console.log(this.username)
   this.http.get('leave/viewLeaveDetails',{username:this.username}).subscribe((res:any)=>{
     console.log(res);
    this.recentdata=res.result;
   })
  }
  

}
