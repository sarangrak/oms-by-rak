import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {formatDate,Location } from '@angular/common';
import { HttpService } from 'src/app/http.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-add-attendance',
  templateUrl: './add-attendance.component.html',
  styleUrls: ['./add-attendance.component.css']
})
export class AddAttendanceComponent implements OnInit {
 
  registerform: FormGroup;
  // showchekout: Boolean = true;
  // showchekin: Boolean = false;
  show: Boolean = true; 
  hide: Boolean = false;
  now:number;
  today: number = Date.now();
  jstoday = '';
  date='';
  getvalue: any;
  first_name: any;
  last_name: any;
  
  username: string;
  emp_id: string;
  today1= new Date();
  constructor(public http:HttpService,private formBuilder: FormBuilder,
    public toastr:ToastrService,public router:Router,public _location:Location,
    private spinner: NgxSpinnerService) {

    setInterval(() => {
      this.now = Date.now();
      this.jstoday = formatDate(this.today1, 'yyyy-MM-dd hh:mm:ss', 'en-US', '+0530');
      
    }, 1
    
    );

    setInterval(() => {this.today = Date.now();
      this.date = formatDate(this.now, 'yyyy-MM-dd', 'en-US', '+0530');
    }, 1);

  
   }

  ngOnInit() {
    this.fetch();
    this.getvalue=[];
    this.first_name="";
    this.last_name="";
    this.viewattend();
    
   
    this.registerform=this.formBuilder.group({
      activity:['',Validators.required]
    })

    this.first_name=localStorage.getItem('first_name');
    this.last_name=localStorage.getItem('last_name');
    
    console.log(this.first_name);
    console.log(this.last_name);
   
  }
 
  
  showchekin()
  {
    this.spinner.show();//show the spinner
    this.http.get('leave/checkattend').subscribe((res:any)=>{
      console.log(res);
      if(res['message']=='already')
      {
        this.toastr.warning('You already apply attendance!', 'Warning!');
      }
      else
      {
        
        const data={
          checkIn:this.jstoday,
          date:this.date,
         // emp_id:this.emp_id
        }
        this.spinner.hide();

        this.spinner.show();//show the spinner
        this.http.post('leave/addAttend',data).subscribe((res:any)=>{
          console.log(res);
          if(res['message']=='Post successfully')
          {

            this.hide=true;
            this.show=false;
            this.toastr.success('You check in successfully!', 'Success!');
          }
          this.spinner.hide();
            })
      }
        })
 
  }

  showchekout()
  {

  // this.hide=false;
  // this.show=true;
  this.emp_id=localStorage.getItem('emp_id');
  
  var date=(this.date);
  
  const data={
    activity:this.registerform.value.activity,
    checkOut:this.jstoday,
    emp_id:this.emp_id
  }
  console.log('h')
  console.log(data)
  this.spinner.show();//show the spinner
  this.http.put('leave/updateAttend',data,{date:date}).subscribe((res:any)=>{
    console.log(res);
     this.hide=false;
   this.show=true;
   if(res['message']=='Updated successfully')
   {
    this.toastr.success('You check out successfully!', 'Success!');
    this.http.get('leave/ShowAttend',{emp_id:this.emp_id}).subscribe((res:any)=>{
      console.log(res);
      this.getvalue=res.result;
    })
   }
   this.spinner.hide();
      })
  //console.log(this.registerform.value);
  
  }


 
  fetch()
  {
    this.emp_id=localStorage.getItem('emp_id');
     console.log(this.emp_id);
    this.http.get('leave/fetchvalue').subscribe((res:any)=>{
      console.log(res);
      if(res['message']=='already exist')
      {
        this.hide=true;
        this.show=false;
      }
      else if(res['message']=='not')
      {
        this.hide=false;
        this.show=true;
        
      }
        })
  }

  viewattend()
  {
    this.http.get('leave/ShowAttend').subscribe((res:any)=>{
      console.log(res);
      this.getvalue=res.result;
    })
  }
 

}
