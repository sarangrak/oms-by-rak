import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeeComponent } from './employee/add-employee/add-employee.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './slizzing/header/header.component';
import { ShowEmployeeComponent } from './employee/show-employee/show-employee.component';
import { SidebarComponent } from './slizzing/sidebar/sidebar.component';
import { LoginComponent } from './login/login/login.component';
import { AddAttendanceComponent } from './attendance/add-attendance/add-attendance.component';
import { ApplyLeaveComponent } from './attendance/apply-leave/apply-leave.component';
import { ShowEmpLeaveComponent } from './employee/show-emp-leave/show-emp-leave.component';
import { EmpLeaveDetailsComponent } from './employee/emp-leave-details/emp-leave-details.component';
import { FormSixteenComponent } from './my-offical-documents/form-sixteen/form-sixteen.component';
import { SalarySlipComponent } from './my-offical-documents/salary-slip/salary-slip.component';
import { CompanyPolicyComponent } from './my-offical-documents/company-policy/company-policy.component';
import { ShowEmpAttendComponent } from './employee/show-emp-attend/show-emp-attend.component';
import { ViewEmpComponent } from './employee/view-emp/view-emp.component';
import { EditEmployeeComponent } from './employee/edit-employee/edit-employee.component';
import { CreatePolicyComponent } from './my-offical-documents/create-policy/create-policy.component';
import { CreateForm16Component } from './my-offical-documents/create-form16/create-form16.component';
import { CreateSalarySlipComponent } from './my-offical-documents/create-salary-slip/create-salary-slip.component';
import { AddEnquiryComponent } from './enquiry/add-enquiry/add-enquiry.component';
import { AddExpenseComponent } from './expense/add-expense/add-expense.component';
import { AuthGuard } from './auth.guard';
import { EditattendComponent } from './employee/editattend/editattend.component';
import { ForgetPassComponent } from './login/forget-pass/forget-pass.component';
import { ShowSalaryComponent } from './my-offical-documents/show-salary/show-salary.component';
import { ViewSalaryComponent } from './my-offical-documents/view-salary/view-salary.component';
import { EditSalaryComponent } from './my-offical-documents/edit-salary/edit-salary.component';
import { GenerateSlipComponent } from './my-offical-documents/generate-slip/generate-slip.component';
import { SalaryHistoryComponent } from './my-offical-documents/salary-history/salary-history.component';
import { HolidayCalenderComponent } from './attendance/holiday-calender/holiday-calender.component';
import { ViewGenSalaryComponent } from './my-offical-documents/view-gen-salary/view-gen-salary.component';
import { UpdategensalaryComponent } from './my-offical-documents/updategensalary/updategensalary.component';
import { ViewEnquiryComponent } from './enquiry/view-enquiry/view-enquiry.component';
import { UpdateEnquiryComponent } from './enquiry/update-enquiry/update-enquiry.component';
import { SalarySheetComponent } from './my-offical-documents/salary-sheet/salary-sheet.component';
import { PdfSalarySlipComponent } from './my-offical-documents/pdf-salary-slip/pdf-salary-slip.component';
import { ChangePassComponent } from './login/change-pass/change-pass.component';
import { ChangepasswordComponent } from './login/changepassword/changepassword.component';
import { ProfileComponent } from './login/profile/profile.component';
import { AddLocationComponent } from './employee/add-location/add-location.component';

//import { ShowAttendanceComponent } from './attendance/show-attendance/show-attendance.component';





const routes: Routes = [

  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'addemp', component: AddEmployeeComponent, pathMatch: 'full',canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent,pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'header', component: HeaderComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'showemp', component: ShowEmployeeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'sidebar', component: SidebarComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'attendance', component: AddAttendanceComponent, canActivate: [AuthGuard] },
  { path: 'applyleave', component: ApplyLeaveComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'showemp-leave', component: ShowEmpLeaveComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'empleave-details', component: EmpLeaveDetailsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'form-sixteen', component: FormSixteenComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'salary-slip', component: SalarySlipComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'policy', component: CompanyPolicyComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'emp-attend', component: ShowEmpAttendComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'view-emp', component: ViewEmpComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'edit-emp', component: EditEmployeeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'create-policy', component: CreatePolicyComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'create-form16', component: CreateForm16Component, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'create-salaryslip', component: CreateSalarySlipComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'add-enquiry', component: AddEnquiryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'add-expense', component: AddExpenseComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'edit-attend', component: EditattendComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'forget-pass', component: ForgetPassComponent, pathMatch: 'full'},
  { path: 'show-salary', component: ShowSalaryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'view-salary', component: ViewSalaryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'edit-salary', component: EditSalaryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'generate-salary', component: GenerateSlipComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'history-salary', component: SalaryHistoryComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'holiday-calender', component: HolidayCalenderComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'view-gen-salary', component: ViewGenSalaryComponent, canActivate: [AuthGuard] },
  { path: 'update-gen-salary', component: UpdategensalaryComponent, canActivate: [AuthGuard] },
  { path: 'view-enquiry', component: ViewEnquiryComponent, canActivate: [AuthGuard] },
  { path: 'update-enquiry', component: UpdateEnquiryComponent, canActivate: [AuthGuard] },
  { path: 'add-salary-sheet', component: SalarySheetComponent, canActivate: [AuthGuard] },
  { path: 'pdf-salary-slip', component: PdfSalarySlipComponent, canActivate: [AuthGuard] },
  { path: 'change-pass/:id', component: ChangePassComponent },
  { path: 'changepassword', component: ChangepasswordComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'add-location', component: AddLocationComponent, canActivate: [AuthGuard] }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
   /// RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })
  ],
  exports: [RouterModule]

})


export class AppRoutingModule { }
